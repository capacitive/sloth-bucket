Sloth Bucket
============

A simple, persistent user slot system to store data sequentially with allocate/deallocate functions.

## Get started

1. Install and run redis.  
2. We have provided a Dockerfile that will create a simple redis server container

  * `sudo docker build -t <your namespace>/redis .`
  * `sudo docker run -d -p 6379:6379 <your namespace>/redis`

3. Install sloth-bucket

  * `npm install sloth-bucket --save`

4. For using sloth-bucket in production, you can easily configure the underlying redis store:

 ```js
 var sloth = require('sloth-bucket');

var options = {
  host: '192.168.2.45',
  port: 8034,
  socket_keepalive: true
}
sloth.init(options);

 ```
## Usage

```js
var sloth = require('sloth-bucket');
var username = 'marvin';

//initialize the slots with a starting buffer:
sloth.createUserSlots(username, 1200, 'infantryUnits');

var hitPoints = '5';
//assign variable value above to a new slot:

sloth.allocateSlotId(username, hitPoints, 'infantryUnits').then((result) => {
  var newId = parseInt(result);
  console.log('NEW ID: ', newId);
});

//deallocate a slot at index 3 for user 'marvin':
sloth.deallocateSlotId(username, 3, 'infantryUnits').then((result) => {
  var newIndex = parseInt(result);
  console.log('DEALLOCATED USER DATA AT INDEX: ', newIndex);
});

sloth.getSlotContentsByIndex(username, 2, 'infantryUnits').then((result) => {
  console.log('GETTING USER DATA AT INDEX 2: ', result);
}

hitPoints = 3;
sloth.setSlotContentsByIndex(username, 2, hitPoints).then((result) => {
  console.log('SETTING USER DATA AT INDEX 2: ', result);
} 
```
## Utilities

```js
//reset all slots for a user:
sloth.deleteUserSlots(username);

//get next available slot id:
sloth.nextAllocationSlot(username, 'infantryUnits').then((result) => {
  console.log('NEXT AVAILABLE ID: ', result);
}); 
```
## Configuration
 For more redis configuration options, consult the [redis](https://www.npmjs.com/package/redis) documentation.

```js
//configure underlying redis cache:
var options = {
  host: '192.168.2.45',
  port: 8034,
  socket_keepalive: true
}
sloth.init(options);

//set slot's hash index as non-zero based:
sloth.nonZeroBased();

```
## Changes by release (all versions prior to 2.0 are beta)
v. 1.4.1
* removed the slot prefixing, in favor of enabling multiple slots, like this:

```js
sloth.createUserSlots(username, 1200, 'infantryUnits');
sloth.createUserSlots(username, 20, 'badgesEarned');

```

v. 1.3.12 
* new minor feature: nonZeroBased() configures non-zero based hash of slots
* new minor feature: get/setSlotValue() function to get/set slot data

v. 1.2.11 
* added nextAvailableSlotId() function for retrieving next unallocated slot Id

## Tests
   ### Set up

1. Install redis on the computer on which you want to run the tests.  
2. We have provided a Dockerfile that will create a simple redis server container
  * `sudo docker build -t <your namespace>/redis .`
  * `sudo docker run -d -p 6379:6379 <your namespace>/redis`
3. Install the redis-cli package:
  * `:$ npm install redis-cli -g`

   ### Run

4. Sloth-bucket tests are simple scripts with no dependencies on unit testing frameworks or test runners.  
   Get over it, cd into the /test folder, run:
   * `node test`
   * `node test1`
   * `node test2`

5. Any time you want to check in on what the tests are producing, run the redis CLI:
  * `:$ redis-cli`
  * `:$ HGETALL user:steve`

6. Bask in the [sloth-bucket] goodness! :)
7. run `node clean` to delete the user slots, or:
  * `:$ redis-cli`
  * `:$ DEL user:steve`


