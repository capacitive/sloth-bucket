var redis = require('redis');
var client = redis.createClient();

client.on('error', function (error) {
	console.log(`Error: ${error}`)
})

var _test = false;
const _nextAvailableSlotId = new Object();
var _indexBase = 0;
var _indexBaseModifier = 0;

var setSlothBucketItem = function (hashName, fieldname, value) {
	var setItemPromise = new Promise(function (resolve, reject) {
		client.hset(hashName, fieldname, value, function (error, object) {
			console.log('[sloth-bucket]: set internal item:', object);
			resolve(object);
		});
	})
	return setItemPromise;
}

var getSlothBucketItem = function (hashName, fieldname, value) {
	var getItemPromise = new Promise(function (resolve, reject) {
		client.hget(hashName, fieldname, function (error, object) {
			console.log('[sloth-bucket]: got internal item:', object);
			resolve(object);
		});
	})
	return getItemPromise;
}

module.exports = {
	createUserSlots: function (username, count, id) {
		var createUserSlotsPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hlen(hashName, function (error, result) {
				if (result === 0) {
					var itemArray = [];
					if (_indexBase > 0) {
						for (let i = 0; i < _indexBaseModifier; i++) {
							itemArray.push('0');
							itemArray.push(JSON.stringify({ allocated: true, canDeallocate: false }));
						}
					}
					for (let i = _indexBase; i < count + _indexBaseModifier; i++) {
						itemArray.push(i + '');
						itemArray.push('{ "allocated": false, "canDeallocate": true, "value": "" }');
					}

					client.hmset(hashName, itemArray, () => {
						client.hvals(hashName, function (error, object) {
							if (_test) {
								console.log('initial hash state for : ' + hashName);
								console.dir(JSON.stringify(object));
								console.log('---------------------------------------');
							}
							if (error) {
								reject("Error creating user slots: ", error);
							} else {
								resolve(object);
							}
						});
					});
				}
			});
		});
		return createUserSlotsPromise;
	},

	allocateSlotId: function (username, contents, id) {
		var allocateTabIdPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hkeys(hashName, function (error, hashlist) {
				var array = [];
				client.hgetall(hashName, (error, object) => {
					array = Object.keys(object).map(key => object[key]);
					array.some((value, i) => {
						var val = JSON.parse(value);
						if (val.allocated === false) {
							if (_test) console.log('[sloth-bucket][%s]: allocated data @ idx: %s', hashName, i);
							client.hset(hashName, i, JSON.stringify({ allocated: true, canDeallocate: true, value: contents }), function (error, result) {
							});
							_nextAvailableSlotId[hashName] = i + 1;
							resolve(i);
							return true;
						}
						//return false;
						//reject('allocateSlotId request REJECTED');
					});
				})
			});
		});

		return allocateTabIdPromise;
	},

	deallocateSlotId: function (username, index, id) {
		var deallocateTabIdPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hkeys(hashName, function (error, hashlist) {
				var array = [];
				client.hgetall(hashName, (error, object) => {
					array = Object.keys(object).map(key => object[key]);
					array.some((val, i) => {
						if (i === index) {
							if (_test) console.log('[sloth-bucket][%s]: deallocated slot @ idx: %s', hashName, i);
							client.hset(hashName, i, JSON.stringify({ allocated: false, canDeallocate: true, value: '' }));
							_nextAvailableSlotId[hashName] = i;
							resolve(i);
							return true;
						} else if (i === index) {
							if (_test) console.log('[sloth-bucket][%s]: REJECTED deallocation @ idx: %s', hashName, i);
							reject("unable to deallocate slot at index: " + index);
							return false;
						}
					});
				});
			});
		});
		return deallocateTabIdPromise;
	},

	getSlotContentsByIndex: function (username, index, id) {
		var getSlotContentPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hgetall(hashName, (error, object) => {
				array = Object.keys(object).map(key => object[key]);
				array.some((val, i) => {
					if (i === index) {
						if (_test) console.log('[sloth-bucket][%s]: getting slot value at idx:', hashName, i);
						client.hget(hashName, i, (error, object) => {
							resolve(object);
						});
						return true;
					} else if (i === index) {
						if (_test) console.log('[sloth-bucket][%s]: getSlotContentsByIndex @ index: %s --- request rejected', hashName, i);
						reject("unable to get slot value at index: " + index);
						return false;
					}
				});
			});
		});
		return getSlotContentPromise;
	},

	setSlotContentsByIndex: function (username, index, value, id) {
		var setSlotContentsByIndexPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hgetall(hashName, (error, object) => {
				array = Object.keys(object).map(key => object[key]);
				array.some((val, i) => {
					if (i === index) {
						if (_test) console.log('[sloth-bucket][%s]: setting slot contents @ idx:', hashName, i);
						client.hset(hashName, i, value, (error, object) => {
							client.hget(hashName, i, (error, object) => {
								resolve(object);
							});
						});
						return true;
					} else if (i === index) {
						console.log('[sloth-bucket] setSlotValue request rejected');
						reject("unable to set slot contents at index: " + index);
						return false;
					}
				});
			});
		});
		return setSlotContentsByIndexPromise;
	},

	setSlotContentsByValue: function (username, oldValue, newValue, id) {
		var setSlotContentsByValuePromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hgetall(hashName, (error, object) => {
				array = Object.keys(object).map(key => object[key]);
				array.some((val, i) => {
					if (val === oldValue) {
						if (_test) console.log('[sloth-bucket][%s]: setting slot contents by value (%s) @ idx:', hashName, oldValue, i);
						client.hset(hashName, i, newValue, (error, object) => {
							client.hget(hashName, i, (error, object) => {
								resolve(object);
							});
						});
						return true;
					}
				});
				reject("unable to set slot contents");
			});
		});
		return setSlotContentsByValuePromise;
	},

	//configuration:
	init: function (options) {
		client.quit();
		client = redis.createClient(options);
	},

	nonZeroBased: function () {
		_indexBase = 1;
		_indexBaseModifier = 1;
	},

	//utility functions:
	deleteUserSlots: function (username, id) {
		var deleteUserSlotsPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.del(hashName, (error, result) => {
				var resultInt = parseInt(result);
				if (error) {
					reject('error deleting userSlots: ', error);
				} else {
					if (_test && resultInt > 0) {
						console.log('[sloth-bucket][%s]: deleted userSlots for: %s', hashName, username);
					} else if (_test) {
						console.log('[sloth-bucket]: %s does not exist.', hashName);
					}
					resolve(result);
				}
			})
		})
		return deleteUserSlotsPromise;
	},

	nextAllocationSlot: function (username, id) {
		var nextAllocationSlotPromise = new Promise(function (resolve, reject) {

			const hashIdentifier = id === undefined ? '' : ':' + id;
			var hashName = 'user:' + username + hashIdentifier;

			client.hkeys(hashName, function (error, hashlist) {
				var array = [];
				client.hgetall(hashName, (error, object) => {
					array = Object.keys(object).map(key => object[key]);
					array.some((value, i) => {
						var val = JSON.parse(value);
						if (val.allocated === false) {
							if (_test) console.log('[sloth-bucket][%s]: nextAllocationSlot is @ idx: %s', hashName, i);
							resolve(i);
							return true;
						}
						//return false;
						//reject('nextAllocationSlot request REJECTED');
					});
				})
			});
		});

		return nextAllocationSlotPromise;
	},

	verbose: function (enableVerboseConsole) {
		_test = enableVerboseConsole;
	}
}



