const sloth = require('../index.js');

sloth.deallocateSlotId('steve', 1).then((result) => {
	sloth.nextAllocationSlot('steve').then((result) => {
		console.log('NEXT AVAILABLE ID: %s (just deallocated %s)', result, 1);
		console.log('');

		sloth.deallocateSlotId('steve', 2).then((result) => {
			sloth.nextAllocationSlot('steve').then((result) => {
				console.log('NEXT AVAILABLE ID: %s (just deallocated %s)', result, 2);
				console.log('');

				sloth.deallocateSlotId('steve', 3).then((result) => {
					sloth.nextAllocationSlot('steve').then((result) => {
						console.log('NEXT AVAILABLE ID: %s (just deallocated %s)', result, 3);
						console.log('');

						sloth.deallocateSlotId('steve', 4).then((result) => {
							sloth.nextAllocationSlot('steve').then((result) => {
								console.log('NEXT AVAILABLE ID: %s (just deallocated %s)', result, 4);
								console.log('');

								sloth.allocateSlotId('steve', 'replaced data @ dealloc slot 1').then((result) => {
									sloth.deallocateSlotId('steve', 5).then((result) => {
										sloth.nextAllocationSlot('steve').then((result) => {
											console.log('NEXT AVAILABLE ID: %s (just allocated %s)', result, 2);
											console.log('');

											sloth.deallocateSlotId('steve', 6).then((result) => {
												sloth.allocateSlotId('steve', 'replaced data @ dealloc slot 2').then((result) => {
													sloth.nextAllocationSlot('steve').then((result) => {
														console.log('NEXT AVAILABLE ID: %s (just allocated %s)', result, 3);
														console.log('');
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});
