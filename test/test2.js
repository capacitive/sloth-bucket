const sloth = require('../index.js');

var dealloc = 7;

sloth.allocateSlotId('steve', 'replaced data @ dealloc slot 3').then((result) => {
	console.log('[promise]-> returned:', result);
	sloth.getSlotContentsByIndex('steve', result).then((result) => {
		console.log('[value]-> ', result);
		console.log('');
		sloth.allocateSlotId('steve', 'data @ slot 6').then((result) => {
			console.log('[promise]-> returned:', result);
			sloth.getSlotContentsByIndex('steve', result).then((result) => {
				console.log('[value]-> ', result);
				console.log('');
				sloth.allocateSlotId('steve', 'data @ slot 7').then((result) => {
					console.log('[promise]-> returned:', result);
					sloth.getSlotContentsByIndex('steve', result).then((result) => {
						console.log('[value]-> ', result);
						console.log('');
						sloth.nextAvailableSlotId('steve').then((result) => {
							console.log('NEXT AVAILABLE ID: %s (should be 8)', result);
							console.log('');
						});
						sloth.deallocateSlotId('steve', dealloc).then((result) => {
							sloth.nextAvailableSlotId('steve').then((result) => {
								console.log('NEXT AVAILABLE ID: %s (should be %s)', result, dealloc);
								console.log('');

								//set some slot values:
								sloth.setSlotContentsByIndex('steve', 6, 'I changed this!!').then((result) => {
									console.log('[value]-> ', result);
									console.log('');
									sloth.getSlotContentsByIndex('steve', 6).then((result) => {
										console.log('[value]-> ', result);
										console.log('');
									})
								});
							});
						});
					});
				});
			});
		});
	});
});