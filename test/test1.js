const sloth = require('../index.js');

var dealloc = 3;

sloth.allocateSlotId('steve', 'replaced data @ dealloc slot 2').then((result) => {
	console.log('[promise]-> returned:', result);
	sloth.getSlotContentsByIndex('steve', result).then((result) => {
		console.log('[value]-> ', result);
		console.log('');
		sloth.allocateSlotId('steve', 'data @ slot 4').then((result) => {
			console.log('[promise]-> returned:', result);
			sloth.getSlotContentsByIndex('steve', result).then((result) => {
				console.log('[value]-> ', result);
				console.log('');

				sloth.allocateSlotId('steve', 'data @ slot 5').then((result) => {
					console.log('[promise]-> returned:', result);
					sloth.getSlotContentsByIndex('steve', result).then((result) => {
						console.log('[value]-> ', result);
						console.log('');

						sloth.nextAvailableSlotId('steve').then((result) => {
							console.log('NEXT AVAILABLE ID: %s (should be 6)', result);
							console.log('');
						});

						sloth.deallocateSlotId('steve', dealloc).then((result) => {

							sloth.nextAvailableSlotId('steve').then((result) => {
								console.log('NEXT AVAILABLE ID: %s (should be %s)', result, dealloc);
								console.log('');
							});
						});
					});
				});
			});
		});
	});
});