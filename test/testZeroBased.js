const sloth = require('../index.js');
sloth.createUserSlots('steve', 'tab', 12);

var counter = 0;
var dealloc = 1;

sloth.allocateSlotId('steve', 'data @ slot 0').then((result) => {
	counter++;
	console.log('[promise]-> returned:', result);
	sloth.getSlotValue('steve', 0).then((result) => {
		console.log('[value]-> ', result);
		console.log('');

		sloth.allocateSlotId('steve', 'data @ slot 1').then((result) => {
			counter++;
			console.log('[promise]-> returned:', result);
			sloth.getSlotValue('steve', 1).then((result) => {
				console.log('[value]-> ', result);
				console.log('');
				sloth.allocateSlotId('steve', 'data @ slot 2').then((result) => {
					counter++;
					console.log('[promise]-> returned:', result);
					sloth.getSlotValue('steve', 2).then((result) => {
						console.log('[value]-> ', result);
						console.log('');
						sloth.nextAvailableSlotId('steve').then((result) => {
							console.log('NEXT AVAILABLE ID: %s (should be %s)', result, counter);
							console.log('');
						});
						sloth.deallocateSlotId('steve', dealloc).then((result) => {
							sloth.nextAvailableSlotId('steve').then((result) => {
								console.log('NEXT AVAILABLE ID: %s (should be %s)', result, dealloc);
								console.log('');
							});
						});
					});
				});
			});
		});
	});
});