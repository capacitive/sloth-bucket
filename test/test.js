const sloth = require('../index.js');
sloth.nonZeroBased();
sloth.createUserSlots('steve', 'tab', 12);

var counter = 0;
var dealloc = 2;

sloth.createUserSlots('steve', 12).then((result) => {
	sloth.allocateSlotId('steve', 'data @ slot 1').then((result) => {
		counter++;
		console.log('allocateSlotId [promise]-> returned:', result);
		sloth.getSlotContentsByIndex('steve', result).then((result) => {
			console.log('getSlotContentsByIndex [value]-> ', result);
			console.log('');

			sloth.allocateSlotId('steve', 'data @ slot 2').then((result) => {
				counter++;
				console.log('allocateSlotId [promise]-> returned:', result);
				sloth.getSlotContentsByIndex('steve', result).then((result) => {
					console.log('getSlotContentsByIndex [value]-> ', result);
					console.log('');
					sloth.allocateSlotId('steve', 'data @ slot 3').then((result) => {
						counter++;
						console.log('allocateSlotId [promise]-> returned:', result);
						sloth.getSlotContentsByIndex('steve', result).then((result) => {
							console.log('getSlotContentsByIndex [value]-> ', result);
							console.log('');
							sloth.nextAvailableSlotId('steve').then((result) => {
								console.log('NEXT AVAILABLE ID: %s (should be %s)', result, counter + 1);
								console.log('');
							});
							sloth.deallocateSlotId('steve', dealloc).then((result) => {
								console.log('deallocateSlotId [value]-> ', result);
								sloth.nextAvailableSlotId('steve').then((result) => {
									console.log('NEXT AVAILABLE ID: %s (should be %s)', result, dealloc);
									console.log('');
								});
							});
						});
					});
				});
			});
		});
	})
});